# Washer Monitor with NodeMCU and MPU6050

## Wiring

|   NodeMCU   |   MPU6050   |
|-------------|-------------|
|     3V3     |     VCC     |
|     GND     |     GND     |
|     D2      |     SDA     |
|     D1      |     SCL     |

## HowTo

### 1. Download VS Code and install on your PC.

### 2. Download driver of NodeMCU and install on your PC

- Mac OS: https://cityos-air.readme.io/docs/installations

- Windows: https://cityos-air.readme.io/docs/windows-os-installation

### Open VS Code and install **PlatformIO** library.

http://docs.platformio.org/en/latest/ide/vscode.html#installation

### Clone this repo and open the downloaded folder on VS Code.
    
http://docs.platformio.org/en/latest/ide/vscode.html 
