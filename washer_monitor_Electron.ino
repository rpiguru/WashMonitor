/*

    Connection Guide
    
    |   Electron   |   MPU6050   |
    ------------------------------
    |     3V3      |     VCC     |
    |     GND      |     GND     |
    |     D0       |     SDA     |
    |     D1       |     SCL     |

*/

#include "MPU6050.h"

// Threshold value/
// The lower value, the more sensitive.(but false detections)
#define MOTION_THRESHOLD        1200    

#define MOTION_DURATION_TIME    4       // Motion duration time in minutes
#define NON_MOTION_TIME         5       // Non-motion time in minutes
#define LED_PIN                 D7      // LED pin number

// MPU variables:
MPU6050 accelgyro;
int16_t ax, ay, az;
int16_t gx, gy, gz;
int16_t old_ax = 0, old_ay = 0, old_az = 0;
int16_t motion;

unsigned long last_detected_time = 0;
unsigned long motion_start_time = 0;
bool is_washing = false;


void setup() {
    pinMode(LED_PIN, OUTPUT);

    Wire.begin();
    Serial.begin(9600);

    Serial.println("Initializing I2C devices...");
    accelgyro.initialize();

    // Cerify the connection:
    Serial.println("Testing device connections...");
    Serial.println(accelgyro.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");
    Particle.publish("STATE", "START");
    delay(1000);
    
}

void loop() {
    
    // read raw accel measurements from device
    accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);

    motion = abs(ax - old_ax) + abs(ay - old_ay) + abs(az - old_az);
    Serial.println(motion);

    if (motion > MOTION_THRESHOLD && !(old_ax == 0 && old_ay == 0 && old_az == 0)) {
        if (motion_start_time == 0) {
            motion_start_time = millis();
        }
        else if (!is_washing && (millis() - motion_start_time > MOTION_DURATION_TIME * 60 * 1000)){
            is_washing = true;
            Particle.publish("twilio_sms", "Washer Started", PRIVATE);
            delay(500);
        }
        digitalWrite(LED_PIN, HIGH);
        last_detected_time = millis();
    }
    else {
        digitalWrite(LED_PIN, LOW);
        if (millis() - last_detected_time > NON_MOTION_TIME * 60 * 1000) {
            if (is_washing){
                Particle.publish("twilio_sms", "Washer Ended", PRIVATE);
                delay(500);
            }
            motion_start_time = 0;
            last_detected_time = 0;
            is_washing = false;
        }
    }

    old_ax = ax; old_ay = ay; old_az = az;
}
