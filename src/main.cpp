/*
    Connection Guide
    
    |   NodeMCU   |   MPU6050   |
    -----------------------------
    |     3V3     |     VCC     |
    |     GND     |     GND     |
    |     D2      |     SDA     |
    |     D1      |     SCL     |

    |     D3      |    TRIG     |

*/

#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#include <Wire.h>
#include <MPU6050.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager
#include <Ticker.h>
#include <ESP8266HTTPClient.h>


#define MOTION_DURATION_TIME    4       // Motion duration time in minutes
#define NON_MOTION_TIME         5       // Non-motion time in minutes
#define MOTION_THRESHOLD        700     // Threshold value to detect motion

#define LED_PIN                 16      // User Built-in LED pin on NodeMCU
#define PING_INTERVAL           5       // Ping Interval in minutes
#define AP_TRIGGER_PIN          D5      // Use D5 to trigger the AP mode


Ticker ticker;

MPU6050 mpu;
int SCL_PIN = D1;
int SDA_PIN = D2;
Vector raw_vector, old_raw_vector;

unsigned long last_detected_time = 0;
unsigned long last_ping_time = 0;
unsigned long motion_start_time = 0;
bool is_washing = false;
bool is_started = false;

HTTPClient http;
String URL_STATUS = "http://api.washdry.us/device/status/set";
String URL_PING = "http://api.washdry.us/device/ping";
String URL_NEW = "http://api.washdry.us/device/new-connection";


void tick(){
  // Toggle state
  int state = digitalRead(LED_PIN);  // get the current state of GPIO16 pin
  digitalWrite(LED_PIN, !state);     // set pin to the opposite state
}


// Gets called when WiFiManager enters configuration mode
void configModeCallback (WiFiManager *myWiFiManager) {
  Serial.println("Entered config mode");
  Serial.println(WiFi.softAPIP());
  // If you used auto generated SSID, print it
  Serial.println(myWiFiManager->getConfigPortalSSID());
  // Entered config mode, make led toggle faster
  ticker.attach(0.2, tick);
}


void initMPU6050(){
  Serial.println("Initialize MPU6050");
  mpu.setAccelPowerOnDelay(MPU6050_DELAY_3MS);

  mpu.setIntFreeFallEnabled(false);  
  mpu.setIntZeroMotionEnabled(false);
  mpu.setIntMotionEnabled(false);
  
  mpu.setDHPFMode(MPU6050_DHPF_5HZ);

  mpu.setMotionDetectionThreshold(2);
  mpu.setMotionDetectionDuration(5);

  mpu.setZeroMotionDetectionThreshold(4);
  mpu.setZeroMotionDetectionDuration(2);	
  
  Serial.println("===== Checking settings of MPU6050 =====");
  
  Serial.print(" * Sleep Mode:                ");
  Serial.println(mpu.getSleepEnabled() ? "Enabled" : "Disabled");
  
  Serial.print(" * Motion Interrupt:          ");
  Serial.println(mpu.getIntMotionEnabled() ? "Enabled" : "Disabled");

  Serial.print(" * Zero Motion Interrupt:     ");
  Serial.println(mpu.getIntZeroMotionEnabled() ? "Enabled" : "Disabled");

  Serial.print(" * Free Fall Interrupt:       ");
  Serial.println(mpu.getIntFreeFallEnabled() ? "Enabled" : "Disabled");
  
  Serial.print(" * Motion Threshold:          ");
  Serial.println(mpu.getMotionDetectionThreshold());

  Serial.print(" * Motion Duration:           ");
  Serial.println(mpu.getMotionDetectionDuration());

  Serial.print(" * Zero Motion Threshold:     ");
  Serial.println(mpu.getZeroMotionDetectionThreshold());

  Serial.print(" * Zero Motion Duration:      ");
  Serial.println(mpu.getZeroMotionDetectionDuration());
  
  Serial.print(" * Clock Source:              ");
  switch(mpu.getClockSource())
  {
    case MPU6050_CLOCK_KEEP_RESET:     Serial.println("Stops the clock and keeps the timing generator in reset"); break;
    case MPU6050_CLOCK_EXTERNAL_19MHZ: Serial.println("PLL with external 19.2MHz reference"); break;
    case MPU6050_CLOCK_EXTERNAL_32KHZ: Serial.println("PLL with external 32.768kHz reference"); break;
    case MPU6050_CLOCK_PLL_ZGYRO:      Serial.println("PLL with Z axis gyroscope reference"); break;
    case MPU6050_CLOCK_PLL_YGYRO:      Serial.println("PLL with Y axis gyroscope reference"); break;
    case MPU6050_CLOCK_PLL_XGYRO:      Serial.println("PLL with X axis gyroscope reference"); break;
    case MPU6050_CLOCK_INTERNAL_8MHZ:  Serial.println("Internal 8MHz oscillator"); break;
  }
  
  Serial.print(" * Accelerometer:             ");
  switch(mpu.getRange())
  {
    case MPU6050_RANGE_16G:            Serial.println("+/- 16 g"); break;
    case MPU6050_RANGE_8G:             Serial.println("+/- 8 g"); break;
    case MPU6050_RANGE_4G:             Serial.println("+/- 4 g"); break;
    case MPU6050_RANGE_2G:             Serial.println("+/- 2 g"); break;
  }  

  Serial.print(" * Accelerometer offsets:     ");
  Serial.print(mpu.getAccelOffsetX());
  Serial.print(" / ");
  Serial.print(mpu.getAccelOffsetY());
  Serial.print(" / ");
  Serial.println(mpu.getAccelOffsetZ());

  Serial.print(" * Accelerometer power delay: ");
  switch(mpu.getAccelPowerOnDelay())
  {
    case MPU6050_DELAY_3MS:            Serial.println("3ms"); break;
    case MPU6050_DELAY_2MS:            Serial.println("2ms"); break;
    case MPU6050_DELAY_1MS:            Serial.println("1ms"); break;
    case MPU6050_NO_DELAY:             Serial.println("0ms"); break;
  }  
  Serial.println();
}


bool postData(String url, String data){
  Serial.println("Sending data, URL: " + url + ", data: " + data);
  http.begin(url);
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");
  int httpCode = http.POST(data);
  String payload = http.getString();
  http.end();
  Serial.println("Return Code: " + String(httpCode) + ", response: " + payload);
  return true;
}


bool sendStatusToServer(String state){
  String data;
  data = "device=" + String(ESP.getChipId()) + "&status=" + state;
  return postData(URL_STATUS, data);
}


bool sendNewConnection(){
  String data;
  data = "device=" + String(ESP.getChipId());
  return postData(URL_NEW, data);
}

bool sendPing(){
  String data;
  data = "device=" + String(ESP.getChipId());
  return postData(URL_PING, data);
}


void setup() {
  // Put your setup code here, to run once:
  Serial.begin(9600);
  
  pinMode(LED_PIN, OUTPUT);
  pinMode(AP_TRIGGER_PIN, INPUT);
  // Start ticker with 0.5 because we start in AP mode and try to connect
  ticker.attach(0.6, tick);

  WiFiManager wifiManager;
  // Set callback that gets called when connecting to previous WiFi fails, and enters Access Point mode
  wifiManager.setAPCallback(configModeCallback);

  // If it does not connect it starts an access point with the specified name here  "AutoConnectAP"
  // and goes into a blocking loop awaiting configuration
  if (!wifiManager.autoConnect()) {
    Serial.println("failed to connect and hit timeout");
    // Reset and try again, or maybe put it to deep sleep
    ESP.reset();
    delay(1000);
  }

  // If you get here you have connected to the WiFi
  Serial.println("connected...yeey :)");
  sendNewConnection();
  ticker.detach();

  int cnt = 0;
  while(!mpu.beginSoftwareI2C(SCL_PIN,SDA_PIN,MPU6050_SCALE_2000DPS, MPU6050_RANGE_16G))
  {
    Serial.println("Could not find a valid MPU6050 sensor, check wiring!");
    // Blink LED
    if (cnt % 2 == 0) digitalWrite(LED_PIN, HIGH);
    else digitalWrite(LED_PIN, LOW);
    cnt++;
    delay(250);
  }
  initMPU6050();

  Serial.println("===== ChipID: " + String(ESP.getChipId()));
  Serial.println();

  ESP.wdtDisable();
  ESP.wdtEnable(WDTO_8S);

}

void loop() {

  if (digitalRead(AP_TRIGGER_PIN) == LOW) {
    Serial.println("Starting another AP now...");
    WiFiManager wifiManager;
    wifiManager.startConfigPortal();
    Serial.println("Connected again... yeey :)");
  }

  if (millis() - last_ping_time > PING_INTERVAL * 60 * 1000){
    sendPing();
    last_ping_time = millis();
  }

  if (!is_started){
    old_raw_vector = mpu.readRawAccel();
    is_started = true;
    return;
  }

  raw_vector = mpu.readRawAccel();
  int offset_raw = abs(raw_vector.XAxis - old_raw_vector.XAxis) + abs(raw_vector.YAxis - old_raw_vector.YAxis) + abs(raw_vector.ZAxis - old_raw_vector.ZAxis);
  // Serial.print("RAW:" ); Serial.println(offset_raw);

  if (offset_raw > MOTION_THRESHOLD){
    Serial.print(millis()); Serial.println(":  Motion is detected!");
    if (motion_start_time == 0) {
      motion_start_time = millis();
    } 
    else if (!is_washing && (millis() - motion_start_time > MOTION_DURATION_TIME * 60 * 1000)){
      is_washing = true;
      Serial.println("===== Washer is running now!");
      sendStatusToServer("running");
    }
    last_detected_time = millis();
    digitalWrite(LED_PIN, LOW);
  } else{
    if (millis() - last_detected_time > NON_MOTION_TIME * 60 * 1000) {
      if (is_washing){
        Serial.println("===== Washer is stopped");
        sendStatusToServer("stopped");
      }
      motion_start_time = 0;
      last_detected_time = 0;
      is_washing = false;
    }
    digitalWrite(LED_PIN, HIGH);
  }
  old_raw_vector = raw_vector;
}
